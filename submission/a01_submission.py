#!/usr/bin/env python
# coding: utf-8

# Image Processing - SCC5830
# Authors: Alexis J. Vargas (Usp Number: 11939710),   Karelia A. Vilca (Usp Number: 11939727)
# Semester 1, Year: 2020
# Assignment 02 - Image Enhancement and Filtering
# gitlab: https://gitlab.com/usp-graphiccomputing/assignment_2_image_enhancement_and_filtering

import numpy as np
import imageio

# GENERAL FUNCTIONS

# - Image padding
#   Adds a zeros outline to the image, the number of zeros is the kernel size divided by 2 (floor)
def padding_img(img,padding):
    """Function that returns padded image
    Arguments:
        img -- input image
        padding -- number of zeros = kernel.shape/2
    """    
    new_img = np.zeros((img.shape[0]+padding*2, img.shape[1]+padding*2))
    ni_n, ni_m = new_img.shape
    new_img[padding:ni_n-padding, padding:ni_m-padding] = img
    return ( new_img )


# - Root Mean Squared Error
#   Rounding to 4 decimal places
def rsr(img1,img2):
    return ( round (np.sqrt(sum(sum((img1 - img2)**2))),4) )

# - Normalization
#   Scaling the image, using normalization(0 - 255), formula considered in the guideline
def normalization(img):
    #return (((img-np.min(img))*255)/(np.max(img)))
    return ((img-np.min(img))/(np.max(img)-np.min(img)))*255 #real normalization

# - Euclidean distance 
#   Simplified for method 1
def E(x, y):
    return np.sqrt(np.power(x,2) + np.power(y,2))
    
# - Gaussian kernel¶
#   Centred at the origin
def G(x,o):
    return (1/(2*np.pi*np.power(o,2)))*np.exp(-1*(np.power(x,2)/(2*np.power(o,2))))

# METHODS

# - Method 1 - Bilateral Filter

# Making the convolution for one pixel
def conv_point(img, n, gs, sigma_r, x, y):
    # a is use to get the range area starting from the pixel and getting the sub image of n x n
    a = int((n-1)/2)
    sub_img = img[ x-a : x+a+1 , y-a:y+a+1 ]
    # calculating the range_kernel, the normalization Factor (Wp) and part of the Bilateral Filter formula
    range_kernel = np.zeros((n,n))
    If = 0
    Wp = 0
    for i in range(n):
        for j in range(n):
            range_kernel[i][j] = G(sub_img[i][j]-img[x][y],sigma_r)
            wi = range_kernel[i][j] * gs[i][j]
            Wp = Wp + wi
            If = If + (wi * sub_img[i,j])
    # Finish the Bilateral Formula dividing the value by the normalization factor
    If = int(If / Wp)
    return If

# Method to run the Full convolution
def M1_bilateral(img,n,sigma_s, sigma_r):
    # Calculate the spatial kernel
    spatial_kernel = np.zeros((n,n))
    for i in range(int(-n/2),int(n/2+1)):
        for j in range(int(-n/2),int(n/2+1)):
            spatial_kernel[i + int(n/2)][j + int(n/2)] = G(E(i,j),sigma_s)
    # Padding the image
    pad = int(np.floor(n/2))
    pad_image = padding_img(img,pad)
    # Make the convolution for all pixels
    result = np.zeros(img.shape)
    for i in range(img.shape[0]):
        for j in range(img.shape[1]):
            result[i][j] = conv_point(pad_image, n, spatial_kernel, sigma_r, i+pad, j+pad)
    return result


# - Method 2 - Unsharp mask using the Laplacian Filter
def M2_unsharp(img,c,k):
    """Function to perform Unsharp mask using the Laplacian Filter
    Arguments:
        img -- input image
        c -- parameter float
        k -- kernel 1 or 2 
    """    
    #0. Choose the kernel and padding the image
    if k == 1:
        kernel = np.matrix([[0,-1, 0], [-1, 4, -1], [0, -1, 0]])
    else:
        kernel = np.matrix([[-1,-1,-1], [-1, 8, -1], [-1, -1, -1]])
    padding = int(np.floor(kernel.shape[0]/2))
    new_img = padding_img(img,padding)    
    #1. Convolving each pixel of the subsection of the image with the chosen kernel
    If = np.zeros(img.shape)
    for i in range(padding, new_img.shape[0] - padding):
        for j in range(padding, new_img.shape[1] - padding):
            sub_img = new_img[ i-1 : i+2 , j-1:j+2 ]
            If[i-padding][j-padding] = np.sum(np.multiply(kernel,sub_img)) 
    #2. Scaling usig normalization          
    If = normalization(If)
    #3. Adding filtered image, multipliyed by parameter c, back to original          
    f = c*If+img
    #4. Scaling usig normalization            
    f = normalization(f)
    return(f)

# - Method 3 - Vignette Filter
def M3_vignette(img,o_r,o_c):
    """Function to perform Vignette Filter
    Arguments:
        img -- input image
        o_r -- row parameter float
        o_c -- col parameter float
    """   
    r,c = img.shape
    # Calculate the central position in rows and columns
    # the even case can be generalized, since ceil will take the upper one if the division is floating in the odd case
    c_r = np.ceil(r/2)-1
    c_c = np.ceil(c/2)-1
    # Fill a 1D kernel,whose size is the number of rows, with consecutive numbers (negatives, 0 and positives centered)
    i = -c_r
    kernel_r = np.zeros(r)
    for a in range(r):
        kernel_r[a]=i
        i=i+1
    # Fill a 1D kernel,whose size is the number of columns, with consecutive numbers (negatives, 0 and positives centered)
    j = -c_c
    kernel_c = np.zeros(c)
    for a in range(c):
        kernel_c[a]=j
        j=j+1
    # Compute gaussian kernels with parameters
    w_row = G(kernel_r,o_r)
    w_col = G(kernel_c,o_c)
    # Transpose kernel_columns and multiply by kernel_rows, result size = input image.
    mul = np.matmul(np.array([w_col]).T,np.array([w_row]))
    # Multiply element by element, the original and resulting image, transpose because of size
    new_img = np.multiply(mul.T,img)
    # Scale using normalization
    new_img = normalization(new_img)
    return(new_img)

# READ INPUT

def readInput():
    filename = input().rstrip()
    method = int(input())
    save = int(input())
    
    img = imageio.imread(filename).astype(np.int32)
    
    if method == 1:
        n = int(input())
        o_s = float(input())
        o_r = float(input())
        output_img = M1_bilateral(img,n,o_s,o_r)
    if method ==2:
        c = float(input())
        k = int(input())
        output_img = M2_unsharp(img,c,k)
    if method ==3:
        o_r = float(input())
        o_c = float(input())
        output_img = M3_vignette(img,o_r,o_c)
    
    rsr_output = rsr(img, output_img)
    output_img = np.round(output_img).astype(np.uint8)
    if save == 1:
        imageio.imwrite('output_img.png',output_img)
    return(rsr_output)

print( readInput() )
