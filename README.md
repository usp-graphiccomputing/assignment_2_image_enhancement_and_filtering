# Assignment 2 Image Enhancement And Filtering

Image Processing - SCC5830. 

Assignment 2 - Image enhancemenet and filtering

Authors:
* Alexis J. Vargas (Usp Number: 11939710)
* Karelia A. Vilca (Usp Number: 11939727)

Semester 1, Year: 2020

Folders and files:
* [Submission](./submission/a01_submission.py) contains the Python code used for run.codes submission
* [Images](/images) contains images used in the demos
* [Notebook with Demo](/notebooks) are the notebooks exemplifying each method developed and submitted
* [Test cases](/testcases) registered test cases
* [Guide](/dip_t02_image_enhancement_filter-vignetting.pdf) theoretical guide